#
# BitBucket
#
#

This is a simple bash script that uses the bitbucket REST API to create, delete, and list 
repositories. I'll add more features soon, but for now that's all it supports. I wanted a 
quick way to programatically create, delete, and list repositories. I couldn't find something
out there that was already written, and didn't require python or something other than a shell so here you go...

Installation
------------

Clone the repo and put the script anywhere you like...

Usage
-----

./bitbucket -c REPO will create 
./bitbucket -d REPO will delete 
./bitbucket -l will list all repos

If you create a file called .bitbucket in your home directory and put the following in it

username:password 

Please set this file to 600! 

The script will not ask you for your username and password 


Function 
--------

The script uses curl as the medium to execute the requests. Checkout http://restbrowser.bitbucket.org/ for more info.


Notes 
-----
Actually now that I'm writing this I just noticed there is another cli tool for bitbucket and 
can be found here: https://bitbucket.org/zhemao/bitbucket-cli

