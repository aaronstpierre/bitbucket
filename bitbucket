#!/bin/bash 
# 
# 
# BitBucket: Script to perform some basic commands at bitbucket using REST API. 
# Currently supports: 
#
# 	CREATE REPO
#	DELETE REPO
# 	List all repos	
#
# Author: Aaron J. St. Pierre <asp@p0v.net> 
#
#
# Reset the optind 
OPTIND=1

#set for your system
CURL=/usr/bin/curl

#functions 
function show_help { 

	echo "./bitbucket [ -c REPONAME ] [ -d REPONAME ] [ -l ]" 
	echo 
	echo "./bitbucket -c REPONAME (will create a repo)" 
	echo "./bitbucket -d REPONAME (will delete a repo)" 
	echo "./bitbucket -l          (will list all repos)"  
}
function create_repo {

	#All repo's are private for me; change if you like" 

	echo "Attempting to create repo $REPO" 

	$CURL -k --user $BBUSERNAME:$BBPASSWORD "https://api.bitbucket.org/1.0/repositories" -d "name=$REPO" -d "is_private=true" > /dev/null 2>&1

	if [ $? -eq 0 ]; then 
		echo "Created repo $REPO" 
		echo ""
		echo "To add files to repository:"
		echo "git remote add origin ssh://git@bitbucket.org/${BBUSERNAME}/${REPO}.git"
	else 
		echo "ERROR: Could not create repo $REPO" 
	fi 	

} 

function delete_repo { 

	echo -n "ARE YOU SURE YOU WANT TO DELETE $REPO? (type: yesimnotanidiot): "
	read resp 

	if [ "${resp}" = "yesimnotanidiot" -o "${resp}" = "yes" ]; then 	
		echo "Attempting to delete repo $REPO"
		$CURL -k -X DELETE --user $BBUSERNAME:$BBPASSWORD "https://api.bitbucket.org/1.0/repositories/${BBUSERNAME}/${REPO}"
		if [ $? -eq 0 ]; then 
			echo "Successfully deleted repo $REPO" 
		else 
			echo "Error: Could not delete repo $REPO" 
		fi 
	else 
		echo "Phew that was a close one...$REPO not deleted." 
		
	fi 
}

function list_repos {

	# list repos 

	# set IFS

	IFS=$'\n' 

	echo "User $BBUSERNAME has the following repositories on bitbucket: " 
	echo 
	for name in `$CURL -s -k --user $BBUSERNAME:$BBPASSWORD "https://api.bitbucket.org/1.0/user/repositories" | grep -i name`
	do 
		reponame=`echo $name | cut -d: -f2 | sed -e 's/\"//g'` 
		echo $reponame
	done	
}	

# First look for a .bitbucket file in homedirectory 
# This file should have the following variable set: BBUSERNAME & BBPASSWORD and file should be 600

BBCONF="${HOME}/.bitbucket" 

if [ -e "${BBCONF}" ]; then 
	BBUSERNAME=`cat $BBCONF | cut -d: -f1`
	BBPASSWORD=`cat $BBCONF | cut -d: -f2`
else 
	# Ask user for username:password
	echo -n "Please enter your bitbucket username: "
	read "BBUSERNAME" 
	echo -n "Please enter your bitbucket password: "
	read -s "BBPASSWORD" 
	echo 
fi 

# Use getopt to parse command line arguments 

# Check ARGC 
if [ $# -eq 0 ]; then 
	show_help 
	exit 1
fi 

while getopts ":c:d:l" opt; do	
	case $opt in 
	c)
		REPO=$OPTARG
		create_repo
		exit 0
		;; 
	d) 	REPO=$OPTARG
		delete_repo
		exit 0 
		;;
	l)	list_repos
		exit 0 
		;;
	:)
		echo "Error: option -$OPTARG requires and argument." 
		show_help
		exit 1 
		;;
	\?)	
		echo "Error: invalid option: -$OPTARG " 
		show_help
		exit 1
		;;
	esac
done 
